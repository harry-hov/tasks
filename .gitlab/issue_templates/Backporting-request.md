<!--
# BACKPORTING REQUEST

GitLab follows a strict maintenance policy. Before proceeding, please read and understand
https://docs.gitlab.com/ee/policy/maintenance.html and https://docs.gitlab.com/ee/policy/maintenance.html#backporting-to-older-releases

This template should be used to report *bug fixes* that should be considered for
backporting up to two releases. It is not required for security fixes.

The best way to ensure that your request is going to be considered is by
filling out all sections of the template appropriately.
-->

<!-- SET THE RIGHT LABELS (let the autocomplete guide you). YOU MUST SET SEVERITY AND PRIORITY LABELS -->
/label ~"devops:: ~"group:: ~"S ~"P
<!-- Assign to the current release managers for review. See https://about.gitlab.com/community/release-managers/ -->
/assign @gitlab-org/release/managers

### Issue link

### MR link

### Latest affected version of GitLab

### Oldest affected version of GitLab

### Does this bug potentially result in data loss?

<!--
DO NOT NAME CUSTOMERS IN THIS ISSUE. LINK TO SALESFORCE OR ZENDESK
You must describe *why* affected customers are not able to upgrade to the latest
version of GitLab. Provide summarized information of customer account size, strategic importance etc.
The context provided here should allow the Release Manager to understand the *impact* of the bug.
Be as brief as possible.
 -->

### Customer impact

<!--
Please explain the impact on customers, such as the risk of not backporting, and
how many customers are expected to be impacted at this time.
-->

### Workaround

<!--
Please explain how one may be able to work around the problems, should the
decision be made to not backport the changes.
-->

/confidential
